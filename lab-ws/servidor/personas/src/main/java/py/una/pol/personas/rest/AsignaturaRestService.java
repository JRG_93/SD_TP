package py.una.pol.personas.rest;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import py.una.pol.personas.model.Asignaturas;
import py.una.pol.personas.model.AsignaturasPersona;
import py.una.pol.personas.model.Persona;
import py.una.pol.personas.service.AsignaturaService;

@Path("/asignaturas")
@RequestScoped
public class AsignaturaRestService {

	@Inject
	AsignaturaService asignaturaService;

	@Inject
	private Logger log;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response crear(Asignaturas p) {

		Response.ResponseBuilder builder = null;

		try {
			asignaturaService.crear(p);
			// Create an "ok" response

			// builder = Response.ok();
			builder = Response.status(201).entity("Asignatura creada exitosamente");

		} catch (SQLException e) {
			// Handle the unique constrain violation
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("bd-error", e.getLocalizedMessage());
			builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
		} catch (Exception e) {
			// Handle generic exceptions
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}

		builder = Response.status(201).entity("Asignatura creada exitosamente");

		return builder.build();
	}

	@DELETE
	@Path("/{nombre}")
	public Response borrar(@PathParam("nombre") String nombre) {
		Response.ResponseBuilder builder = null;
		try {
			asignaturaService.borrar(nombre);
			builder = Response.status(202).entity("Persona borrada exitosamente.");

		} catch (SQLException e) {
			// Handle the unique constrain violation
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("bd-error", e.getLocalizedMessage());
			builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
		} catch (Exception e) {
			// Handle generic exceptions
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}
		
		builder = Response.status(202).entity("Persona borrada exitosamente.");

		return builder.build();
	}

	@POST
	@Path("/inscribirAlumno")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response inscribirAlumno(AsignaturasPersona p) {

		Response.ResponseBuilder builder = null;

		try {
			asignaturaService.inscribirAlumno(p);
			// Create an "ok" response

			// builder = Response.ok();
			builder = Response.status(201).entity("Alumno inscripto creada exitosamente");

		} catch (SQLException e) {
			// Handle the unique constrain violation
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("bd-error", e.getLocalizedMessage());
			builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
		} catch (Exception e) {
			// Handle generic exceptions
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}

		builder = Response.status(201).entity("Alumno inscripto creada exitosamente");

		return builder.build();
	}

	@DELETE
	@Path("/borrarAlumno/{nombre}/{cedula}")
	public Response borrar(@PathParam("nombre") String nombre, @PathParam("cedula") Long ceudla) {
		Response.ResponseBuilder builder = null;
		try {
			AsignaturasPersona p = new AsignaturasPersona();
			p.setAsignatura(nombre);
			p.setCi(ceudla);
			asignaturaService.borrarAlumno(p);
			builder = Response.status(202).entity("Persona borrada exitosamente.");

		} catch (SQLException e) {
			// Handle the unique constrain violation
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("bd-error", e.getLocalizedMessage());
			builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
		} catch (Exception e) {
			// Handle generic exceptions
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}
		return builder.build();
	}

	@GET
	@Path("/listar")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Asignaturas> listar() {
		List<Asignaturas> p = asignaturaService.seleccionar();
		if (p == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		return p;
	}

	@GET
	@Path("/listarAlumnos/{nombre}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Persona> listarAlumnos(@PathParam("nombre") String nombre) throws Exception {
		List<Persona> p = asignaturaService.listarAlumnos(nombre);
		if (p == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		return p;
	}

	@GET
	@Path("/listarurasAsinat/{ci}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Asignaturas> listarAlumnos(@PathParam("ci") Long ci) throws Exception {
		List<Asignaturas> p = asignaturaService.listarAsignaturas(ci);
		if (p == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		return p;
	}
}
