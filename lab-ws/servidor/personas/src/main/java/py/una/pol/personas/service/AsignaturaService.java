package py.una.pol.personas.service;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.dao.AsignaturaDAO;
import py.una.pol.personas.model.Asignaturas;
import py.una.pol.personas.model.AsignaturasPersona;
import py.una.pol.personas.model.Persona;

@Stateless
public class AsignaturaService {
	@Inject
	private Logger log;

	@Inject
	private AsignaturaDAO dao;

	public void crear(Asignaturas p) throws Exception {
		log.info("Creando asignatura: " + p.getNombre() + " " + p.getDescripcion());
		try {
			dao.insertar(p);
		} catch (Exception e) {
			log.severe("ERROR al crear asignatura: " + e.getLocalizedMessage());
			throw e;
		}
		log.info("Asignatura creada con éxito: " + p.getNombre() + " " + p.getDescripcion());
	}

	public List<Asignaturas> seleccionar() {
		return dao.seleccionar();
	}

	public long borrar(String nombre) throws Exception {
		return dao.borrar(nombre);
	}

	public long inscribirAlumno(AsignaturasPersona p) throws Exception {
		return dao.inscribirAlumno(p);
	}

	public long borrarAlumno(AsignaturasPersona p) throws Exception {
		return dao.inscribirAlumno(p);
	}

	public List<Persona> listarAlumnos(String nombre) throws Exception {
		return dao.listarAlumnos(nombre);
	}

	public List<Asignaturas> listarAsignaturas(Long ci) {
		return dao.listarAsignaturas(ci);
	}
}
