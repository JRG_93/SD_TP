package py.una.pol.personas.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.model.Asignaturas;
import py.una.pol.personas.model.AsignaturasPersona;
import py.una.pol.personas.model.Persona;

@Stateless
public class AsignaturaDAO {
	@Inject
	private Logger log;

	/**
	 * 
	 * @param condiciones
	 * @return
	 */
	public List<Asignaturas> seleccionar() {
		String query = "SELECT nombre,descripcion FROM asignatura ";

		List<Asignaturas> lista = new ArrayList<Asignaturas>();

		Connection conn = null;
		try {
			conn = Bd.connect();
			ResultSet rs = conn.createStatement().executeQuery(query);

			while (rs.next()) {
				Asignaturas p = new Asignaturas();
				p.setNombre(rs.getString(1));
				p.setDescripcion(rs.getString(2));
				lista.add(p);
			}

		} catch (SQLException ex) {
			log.severe("Error en la seleccion: " + ex.getMessage());
		} finally {
			try {
				conn.close();
			} catch (Exception ef) {
				log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
			}
		}
		return lista;
	}

	public long insertar(Asignaturas p) throws SQLException {

		String SQL = "INSERT INTO asignatura(nombre, descripcion) " + "VALUES(?,?)";

		long id = 0;
		Connection conn = null;

		try {
			conn = Bd.connect();
			PreparedStatement pstmt = conn.prepareStatement(SQL);
			pstmt.setString(1, p.getNombre());
			pstmt.setString(2, p.getDescripcion());

			int affectedRows = pstmt.executeUpdate();
			// check the affected rows
			if (affectedRows > 0) {
				// get the ID back
				id = 1;

			}
		} catch (SQLException ex) {
			throw ex;
		} finally {
			try {
				conn.close();
			} catch (Exception ef) {
				log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
			}
		}

		return id;

	}

	public long actualizar(Asignaturas p) throws SQLException {

		String SQL = "UPDATE asignatura SET descripcion = ? WHERE nombre = ? ";

		long id = 0;
		Connection conn = null;

		try {
			conn = Bd.connect();
			PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, p.getDescripcion());
			pstmt.setString(2, p.getNombre());

			int affectedRows = pstmt.executeUpdate();
			// check the affected rows
			if (affectedRows > 0) {
				// get the ID back

				id = 1;

			}
		} catch (SQLException ex) {
			log.severe("Error en la actualizacion: " + ex.getMessage());
		} finally {
			try {
				conn.close();
			} catch (Exception ef) {
				log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
			}
		}
		return id;
	}

	public long borrar(String nombre) throws SQLException {

		String SQL = "DELETE FROM asignatura WHERE nombre = ? ";

		long id = 0;
		Connection conn = null;

		try {
			conn = Bd.connect();
			PreparedStatement pstmt = conn.prepareStatement(SQL);
			pstmt.setString(1, nombre);

			int affectedRows = pstmt.executeUpdate();
			// check the affected rows
			if (affectedRows > 0) {
				// get the ID back

				id = 1;

			}
		} catch (SQLException ex) {
			log.severe("Error en la eliminación: " + ex.getMessage());
			throw ex;
		} finally {
			try {
				conn.close();
			} catch (Exception ef) {
				log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
				throw ef;
			}
		}
		return id;
	}

	public long inscribirAlumno(AsignaturasPersona p) throws SQLException {
		String SQL = "INSERT INTO asignatura_alumno (ci, nombre) " + "VALUES(?,?)";

		long id = 0;
		Connection conn = null;

		try {
			conn = Bd.connect();
			PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
			pstmt.setLong(1, p.getCi());
			pstmt.setString(2, p.getAsignatura());

			int affectedRows = pstmt.executeUpdate();
			// check the affected rows
			if (affectedRows > 0) {
				// get the ID back
				id = 1;

			}
		} catch (SQLException ex) {
			throw ex;
		} finally {
			try {
				conn.close();
			} catch (Exception ef) {
				log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
			}
		}

		return id;

	}

	public long borrarAlumno(AsignaturasPersona p) throws SQLException {

		String SQL = "DELETE FROM asignatura_alumno WHERE ci = ? and nombre=? ";

		long id = 0;
		Connection conn = null;

		try {
			conn = Bd.connect();
			PreparedStatement pstmt = conn.prepareStatement(SQL);
			pstmt.setLong(1, p.getCi());
			pstmt.setString(2, p.getAsignatura());

			int affectedRows = pstmt.executeUpdate();
			// check the affected rows
			if (affectedRows > 0) {
				// get the ID back
				id = 1;
			}
		} catch (SQLException ex) {
			log.severe("Error en la eliminación: " + ex.getMessage());
			throw ex;
		} finally {
			try {
				conn.close();
			} catch (Exception ef) {
				log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
				throw ef;
			}
		}
		return id;
	}

	public List<Persona> listarAlumnos(String nombre) {
		String query = "select p.cedula, p.nombre, p.apellido from asignatura_alumno aa join persona p on p.cedula = aa.ci "
				+ "where aa.nombre = '" + nombre + "'";

		List<Persona> lista = new ArrayList<Persona>();

		Connection conn = null;
		try {
			conn = Bd.connect();
			ResultSet rs = conn.createStatement().executeQuery(query);

			while (rs.next()) {
				Persona p = new Persona();
				p.setCedula(rs.getLong(1));
				p.setNombre(rs.getString(2));
				p.setApellido(rs.getString(3));
				lista.add(p);
			}

		} catch (SQLException ex) {
			log.severe("Error en la seleccion: " + ex.getMessage());
		} finally {
			try {
				conn.close();
			} catch (Exception ef) {
				log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
			}
		}
		return lista;
	}

	public List<Asignaturas> listarAsignaturas(Long ci) {
		String query = "select p.nombre, p.descripcion from asignatura_alumno aa join asignatura p on p.nombre = aa.nombre "
				+ "where aa.ci = " + ci;

		List<Asignaturas> lista = new ArrayList<Asignaturas>();

		Connection conn = null;
		try {
			conn = Bd.connect();
			ResultSet rs = conn.createStatement().executeQuery(query);

			while (rs.next()) {
				Asignaturas p = new Asignaturas();
				p.setNombre(rs.getString(1));
				p.setDescripcion(rs.getString(2));
				lista.add(p);
			}

		} catch (SQLException ex) {
			log.severe("Error en la seleccion: " + ex.getMessage());
		} finally {
			try {
				conn.close();
			} catch (Exception ef) {
				log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
			}
		}
		return lista;
	}

}
